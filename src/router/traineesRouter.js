import { Router } from "express";

let traineesRouter = Router();

traineesRouter
  .route("/") //localhost:8000/bikes
  .post((req, res, next) => {
    res.json("bike post");
  })
  .get((req, res, next) => {
    res.json("bike get");
  })
  .patch((req, res, next) => {
    res.json("bike patch");
  })
  .delete((req, res, next) => {
    res.json("bike delete");
  });

export default traineesRouter;

/* 
      url=localhost:8000/bikes,post at response "bike post"
			url=localhost:8000/bikes,get at response "bike get"
			url=localhost:8000/bikes,patch at response "bike up"
			url=localhost:8000/bikes,delete at response "bike delete"

*/
