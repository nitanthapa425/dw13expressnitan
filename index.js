import express, { json } from "express";
import traineesRouter from "./src/router/traineesRouter.js";
import bikeRouter from "./src/router/bikeRouter.js";
import connectToMongoDb from "./src/connectdb/connectToMongodb.js";
import studentRouter from "./src/router/studentRouter.js";

// make express application
let expressApp = express();
expressApp.use(json()); // this code makes our system  to take json data , always place express.use(json()) just below expressApp
// attach port to that
connectToMongoDb();

// expressApp.use(
//   (req, res, next) => {
//     console.log("i am application middleware1");
//     next();
//   },
//   (req, res, next) => {
//     console.log("i am application middleware2");
//     next();
//   }
// );

expressApp.use("/trainees", traineesRouter); //localhost:8000/bikes
expressApp.use("/bikes", bikeRouter); //localhost:8000/bikes
expressApp.use("/students", studentRouter); //localhost:8000/bikes

expressApp.listen(8000, () => {
  console.log("express application is listening at port 8000");
});

/* 
      url=localhost:8000/bikes,post at response "bike post"
			url=localhost:8000/bikes,get at response "bike get"
			url=localhost:8000/bikes,patch at response "bike patch"
			url=localhost:8000/bikes,delete at response "bike delete"

*/
